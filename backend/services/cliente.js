const connect = require("./config-bd");

async function insertar(cliente) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "insert into tbl_cliente(nombre,nit,direccion,fecha_creado) values(";
            sql = sql + "'" + cliente.nombre + "', ";
            sql = sql + "'" + cliente.nit + "',";
            sql = sql + "'" + cliente.direccion + "',";
            sql = sql + "now() ) ";
            conn.query(sql, function(error, resultado) {    
                console.log(resultado);            
                if(resultado.affectedRows === 1){
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function listar() {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "select clienteid, nombre,nit,direccion,fecha_creado from tbl_cliente";
            conn.query(sql, function(error, filas) {
                resolve({lista: filas});
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function actualizar(cliente) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "update tbl_cliente set ";
            sql = sql + "nombre='" + cliente.nombre + "', ";
            sql = sql + "nit='" + cliente.nit + "',";
            sql = sql + "direccion='" + cliente.direccion + "' ";
            sql = sql + "where clienteid=" + cliente.clienteid;

            conn.query(sql, function(error, resultado) {
                if(resultado.affectedRows === 1) {
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

function eliminar() {
    return true;
}

exports.insertar = insertar;
exports.listar = listar;
exports.actualizar = actualizar;
exports.eliminar = eliminar;
