var mysql=require('mysql');

var conn=mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'',
    database:'test'
});

function connect() {
    conn.connect(function (error) {
        if (error) {
            console.log('Ocurrió un problema con la conexion a base de datos');
        } else {
            console.log('se inició la conexion');
        }
    });
    return conn;
}


module.exports=connect;