const connect = require("./config-bd");

async function insertar(venta) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "insert into tbl_venta(clienteid, total, fecha_creado) values(";
            sql = sql + venta.clienteid + ", ";
            sql = sql + venta.total + ", ";
            sql = sql + "now() ) ";
            conn.query(sql, function(error, resultado) {
                if(resultado.affectedRows === 1){
                    let ventaid = resultado.insertId;

                    let queryDet = "insert into tbl_venta_det(ventaid,ventadetid,productoid,precio,cantidad) ";

                    // venta.detalle.forEach(element => {
                    for(let i=0; i<venta.detalle.length; i++){
                        const item = venta.detalle[i];
                        queryDet = queryDet + "select " + ventaid + ",";
                        queryDet = queryDet + (i+1) + ",";
                        queryDet = queryDet + item.productoid + ",";
                        queryDet = queryDet + item.precio + ",";
                        queryDet = queryDet + item.cantidad + " ";

                        if(i !== (venta.detalle.length-1)){
                            queryDet = queryDet + "union ";
                        }
                    };
                    
                    conn.query(queryDet, function(error, resultado) {
                        console.log(error, resultado);
                        if(resultado.affectedRows === venta.detalle.length) {
                            resolve(true);
                        } else {
                            reject(false);
                        }
                    });
                } else {
                    reject(false);
                }
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function listarVentaEnc() {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "select a.ventaid, a.clienteid, b.nombre, b.nit, a.total, a.fecha_creado ";
            sql = sql + "from tbl_venta a ";
            sql = sql + "inner join tbl_cliente b ";
            sql = sql + "on a.clienteid = b.clienteid ";
            
            conn.query(sql, function(error, filas) {
                resolve({lista: filas});
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function listarVentaDet(venta) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "select a.ventaid, a.ventadetid, a.productoid, b.nombre, a.precio, a.cantidad ";
            sql = sql + "from tbl_venta_det a ";
            sql = sql + "inner join tbl_producto b ";
            sql = sql + "on a.productoid = b.productoid ";
            sql = sql + "where ventaid = " + venta.ventaid;
            
            conn.query(sql, function(error, filas) {
                resolve({lista: filas});
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

exports.insertar = insertar;
exports.listarVentaEnc = listarVentaEnc;
exports.listarVentaDet = listarVentaDet;
