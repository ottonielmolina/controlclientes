const connect = require("./config-bd");

async function insertar(existencia) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();

            let sqlValida = "select count(1) as conteo from tbl_existencia where productoid=" + existencia.productoid;
            conn.query(sqlValida, function(error, filas) {
                console.log(filas);
                let sql = "";
                if(filas[0].conteo === 0) {
                    sql = "insert into tbl_existencia(productoid, existencia) values(";
                    sql = sql + existencia.productoid + ", ";
                    sql = sql + existencia.existencia + " ) ";
                } else {
                    sql = "update tbl_existencia set ";
                    sql = sql + "existencia=(existencia+" + existencia.existencia + ") ";
                    sql = sql + "where productoid=" + existencia.productoid; 
                }
                conn.query(sql, function(error, resultado) {
                    if(resultado.affectedRows === 1) {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                });
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function listar() {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "select a.productoid, b.nombre, a.existencia ";
            sql = sql + "from tbl_existencia a inner join tbl_producto b on a.productoid = b.productoid ";
            conn.query(sql, function(error, filas) {
                resolve({lista: filas});
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function actualizar(existencia) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "update tbl_existencia set ";
            sql = sql + "existencia=" + existencia.existencia + " ";
            sql = sql + "where productoid=" + existencia.productoid;

            conn.query(sql, function(error, resultado) {
                if(resultado.affectedRows === 1) {
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

function eliminar() {
    return true;
}

exports.insertar = insertar;
exports.listar = listar;
exports.actualizar = actualizar;
exports.eliminar = eliminar;
