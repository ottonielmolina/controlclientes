const connect = require("./config-bd");

async function insertar(producto) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "insert into tbl_producto(nombre,descripcion,precio_unitario,fecha_creado) values(";
            sql = sql + "'" + producto.nombre + "', ";
            sql = sql + "'" + producto.descripcion + "',";
            sql = sql + "'" + producto.precio_unitario + "',";
            sql = sql + "now() ) ";
            conn.query(sql, function(error, resultado) {
                if(resultado.affectedRows === 1){
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function listar() {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "select productoid,nombre,descripcion,precio_unitario,fecha_creado from tbl_producto ";
            conn.query(sql, function(error, filas) {
                resolve({lista: filas});
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

async function actualizar(producto) {
    return new Promise(function(resolve, reject) {
        let conn;
        try{
            conn = connect();
            let sql = "update tbl_producto set ";
            sql = sql + "nombre='" + producto.nombre + "', ";
            sql = sql + "descripcion='" + producto.descripcion + "',";
            sql = sql + "precio_unitario='" + producto.precio_unitario + "' ";
            sql = sql + "where productoid=" + producto.productoid;

            conn.query(sql, function(error, resultado) {
                if(resultado.affectedRows === 1) {
                    resolve(true);
                } else {
                    reject(false);
                }
            });
        } catch(err){
            console.log(err);
            reject(false);
        }
    });
}

function eliminar() {
    return true;
}

exports.insertar = insertar;
exports.listar = listar;
exports.actualizar = actualizar;
exports.eliminar = eliminar;
