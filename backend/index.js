const cors = require('cors')
const express = require("express");
const cliente = require("./services/cliente");
const producto = require("./services/producto");
const existencia = require("./services/existencia");
const venta = require("./services/venta");

const bodyParser = require('body-parser');
const app = express();
const puerto = 3100;
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let respuesta = {
    codigo: 0,
    mensaje: 'OK',
    data: null
}

function setMensaje(codigo = 0, mensaje = 'OK', data = null) {
    respuesta.codigo = codigo;
    respuesta.mensaje = mensaje,
    respuesta.data = data;
    return respuesta;
}

function setMensajeError() {
    respuesta.codigo = 1;
    respuesta.mensaje = 'Ocurrió un error al procesar la petición solicitada, reintente más tarde',
    respuesta.data = null;
    return respuesta;
}

app.route('/api-cliente/clientes')
    .post(function(req, res) {
        cliente.insertar(req.body).then( result => {
            // console.log(result);
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .get(function(req, res) {
        cliente.listar().then( result => {
            // console.log(result);
            res.send(setMensaje(0,'OK',result));
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .put(function(req, res) {
        cliente.actualizar(req.body).then( result => {
            // console.log(result);
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .delete(function(req, res) {
    });

app.route('/api-cliente/producto')
    .post(function(req, res) {
        producto.insertar(req.body).then( result => {
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .get(function(req, res) {
        producto.listar().then( result => {
            // console.log(result);
            res.send(setMensaje(0,'OK',result));
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .put(function(req, res) {
        producto.actualizar(req.body).then( result => {
            // console.log(result);
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .delete(function(req, res) {
    });

app.route('/api-cliente/existencia')
    .post(function(req, res) {
        existencia.insertar(req.body).then( result => {
            // console.log(result);
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .get(function(req, res) {
        existencia.listar().then( result => {
            // console.log(result);
            res.send(setMensaje(0,'OK',result));
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .put(function(req, res) {
        existencia.actualizar(req.body).then( result => {
            // console.log(result);
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    });

app.route('/api-cliente/venta')
    .post(function(req, res) {
        venta.insertar(req.body).then( result => {
            // console.log(result);
            res.send(setMensaje());
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    })
    .get(function(req, res) {
        venta.listarVentaEnc().then( result => {
            // console.log(result);
            res.send(setMensaje(0,'OK',result));
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    // })
    // .put(function(req, res) {
    //     existencia.actualizar(req.body).then( result => {
    //         // console.log(result);
    //         res.send(setMensaje());
    //     }).catch(err => {
    //         console.log(err);
    //         res.send(setMensajeError());
    //     });
    });

app.route('/api-cliente/venta-det')
    .get(function(req, res) {
        venta.listarVentaDet(req.query).then( result => {
            // console.log(result);
            res.send(setMensaje(0,'OK',result));
        }).catch(err => {
            console.log(err);
            res.send(setMensajeError());
        });
    });

app.listen(puerto, () => {
    console.log("El servidor ha iniciado en el puerto " + puerto);
});