import React from 'react';
import ReactDOM from 'react-dom';  
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'  
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Productos from './pages/productos/productos';
import Clientes from './pages/clientes/clientes';
import Existencias from './pages/existencias/existencias';
import Venta from './pages/venta/venta';
import { Toolbar, Drawer, List, ListItem, ListItemText, ListItemIcon } from '@material-ui/core';
import Headers from './header/header';

import ListAltIcon from '@material-ui/icons/ListAlt';
import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import DescriptionIcon from '@material-ui/icons/Description';
import ReceiptIcon from '@material-ui/icons/Receipt';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Headers />
      <nav aria-label="">
        <Drawer className="drawer" variant="permanent">
          <Toolbar />
          <List style={{width: 180}}>
            <ListItem button component={Link} to="/">
              <ListItemIcon><HomeIcon /> </ListItemIcon>
              <ListItemText primary="Inicio" />
            </ListItem>
            <ListItem button component={Link} to="/productos">
              <ListItemIcon><ListAltIcon /> </ListItemIcon>
              <ListItemText primary="Productos" />
            </ListItem>
            <ListItem button component={Link} to="/clientes">
              <ListItemIcon><PersonIcon /> </ListItemIcon>
              <ListItemText primary="Clientes" />
            </ListItem>
            <ListItem button component={Link} to="/existencias">
              <ListItemIcon><DescriptionIcon /> </ListItemIcon>
              <ListItemText primary="Existencias" />
            </ListItem>
            <ListItem button component={Link} to="/ventas">
              <ListItemIcon><ReceiptIcon /> </ListItemIcon>
              <ListItemText primary="Ventas" />
            </ListItem>
          </List>
        </Drawer>
      </nav>
      <div style={{flexGrow: 1, marginTop: 70, marginLeft: 200, marginRight: 30}}>
        <Route exact path="/" component={App} />  
        <Route path="/productos" component={Productos} />
        <Route path="/clientes" component={Clientes} />
        <Route path="/existencias" component={Existencias} />
        <Route path="/ventas" component={Venta} />
      </div>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
