import React from 'react';
import { makeStyles, AppBar, Toolbar, Typography } from "@material-ui/core";
import CssBaseline from '@material-ui/core/CssBaseline';

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  }
}));

export default function Headers() {
  const classes = useStyles();

  return(
    <div>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            Tienda
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

