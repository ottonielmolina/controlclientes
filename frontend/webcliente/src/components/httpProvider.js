
class httpProvider {
    post(url, data) {
        return new Promise((resolve, reject) => {
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            };
            fetch(url, requestOptions)
            .then(response => response.json())
            .then((response) => {
                console.log(response);
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        });
    };

    get(url) {
        return new Promise((resolve, reject) => {
            const requestOptions = {
                method: 'GET',
                headers: { 'Content-Type': 'none' }
            };
            console.log(url);
            fetch(url, requestOptions)
            .then((response) => {
                console.log(response);
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        });
    }

    put(url, data) {
        return new Promise((resolve, reject) => {
            const requestOptions = {
                method: 'PUT',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(data)
            };
            fetch(url, requestOptions)
            .then(response => response.json())
            .then((response) => {
                console.log(response);
                resolve(response);
            }).catch((error) => {
                reject(error);
            });
        });
    }

}

export default httpProvider;
