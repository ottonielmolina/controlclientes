import React from 'react';
import Paper from '@material-ui/core/Paper';
import { Button, TableContainer, TableHead, Table, TableRow, TableCell, TableBody, Divider, Dialog, DialogTitle, DialogContent, 
  TextField, DialogActions, IconButton } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import httpProvider from '../../components/httpProvider';

const url = "http://localhost:3100/api-cliente/clientes";

class Clientes extends React.Component {  
  http = new httpProvider();
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
      show: false,
      openNot: false,
      ormCliente: { nombre: '', nit: '', direccion: '' }
    };
    this.btnNuevo = this.btnNuevo.bind(this);
    this.btnEditar = this.btnEditar.bind(this);
    this.handleNombre = this.handleNombre.bind(this);
    this.handleNit = this.handleNit.bind(this);
    this.handleDireccion = this.handleDireccion.bind(this);
  }

  componentWillMount() {
    this.http.get(url).then(
      res => res.json()
    ).then((result) => {
      console.log(result);
      if(result.codigo === 0) {
        this.setState({
          isLoaded: true,
          data: result.data.lista
        });
      } else {
        this.setState({
          isLoaded: true,
          error: result.mensaje
        });
      }
    },
    (error) => {
      this.setState({
        isLoaded: true,
        error
      });
    });
  }

  render() {

    const { error, isLoaded, data } = this.state;
    if(error){
      return (<div> {error.message}</div>);
    } else if(!isLoaded){
      return (<CircularProgress />);
    } else {
      return (
        <div>
          <h1>Clientes</h1>
          <Button variant="contained" startIcon={<AddIcon />} onClick={() => {this.btnNuevo()}}>Nuevo</Button>
          <NotificationContainer />
          <Divider />
          <TableContainer component={Paper}>
            <Table style={{minWidth: 400}} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Nit</TableCell>
                  <TableCell>Direccion</TableCell>
                  <TableCell align="center">Creado</TableCell>
                  <TableCell align="center">Opciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row) => (
                  <TableRow key={row.clienteid}>
                    <TableCell component="th" scope="row">{row.clienteid}</TableCell>
                    <TableCell>{row.nombre}</TableCell>
                    <TableCell>{row.nit}</TableCell>
                    <TableCell>{row.direccion}</TableCell>
                    <TableCell align="center">{row.fecha_creado}</TableCell>
                    <TableCell align="center">
                      <IconButton onClick={() => {this.btnEditar(row)}}><EditIcon /></IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          
          <Dialog open={this.state.show} onClose={this.handleClose.bind(this)} aria-labelledby="form-dialog-title"
              disableBackdropClick disableEscapeKeyDown>
            <DialogTitle id="form-dialog-title">{this.state.formTitle}</DialogTitle>
            <DialogContent>
              <form>
                <div>
                  <TextField autoFocus value={this.state.ormCliente.nombre} name="txtNombre"
                      onChange={this.handleNombre} id="txtNombre" label="Nombre Cliente" type="text" />
                </div><br/>
                <div>
                  <TextField value={this.state.ormCliente.nit} name="txtNit"
                      onChange={this.handleNit} id="txtNit" label="NIT" type="text" />
                </div><br/>
                <div>
                  <TextField value={this.state.ormCliente.direccion} name="txtDireccion"
                      onChange={this.handleDireccion} id="txtDireccion" label="Dirección" type="text" />
                </div>
              </form>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.guardarCliente.bind(this)} color="primary">
                Guardar
              </Button>
              <Button onClick={this.handleClose.bind(this)} color="primary">
                Cerrar
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
  }


  handleNombre(e) {
    let prod = this.state.ormCliente;
    prod.nombre = e.target.value;
    this.setState({
      ormCliente: prod
    });
  }
  handleNit(e) {
    let prod = this.state.ormCliente;
    prod.nit = e.target.value;
    this.setState({
      ormCliente: prod
    });
  }
  handleDireccion(e) {
    let prod = this.state.ormCliente;
    prod.direccion = e.target.value;
    this.setState({
      ormCliente: prod
    });
  }

  handleClose() {
    this.setState({show: false});
  }

  btnNuevo() {
    this.setState({
      formTitle: 'Nuevo Registro',
      show: true,
      ormCliente: { nombre: '', nit: '', direccion: '' }
    });
  }

  btnEditar(row) {
    let registro = {
      clienteid: row.clienteid,
      nombre: row.nombre,
      nit: row.nit,
      direccion: row.direccion
    };
    this.setState({
      formTitle: 'Modificar Registro',
      show: true,
      ormCliente: registro
    });
  }

  guardarCliente() {
    const orm = this.state.ormCliente;

    let promesa;
    if(orm.clienteid) {
      console.log('put');
      promesa = this.http.put(url, orm);
    } else {
      console.log('post');
      promesa = this.http.post(url, orm);
    }

    promesa.then((result) => {
      console.log(result);
      if(result.codigo === 0) {
        NotificationManager.success('Información guardada exitosamente.');
        this.componentWillMount();
        this.handleClose();
      } else {
        NotificationManager.error(result.mensaje);
      }
    }, (error) => {
      console.log(error);
      NotificationManager.error('Ocurrió un evento inesperado.');
    });
  }
}  
export default Clientes;
