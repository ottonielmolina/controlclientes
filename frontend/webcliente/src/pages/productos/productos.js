import React from 'react';
import Paper from '@material-ui/core/Paper';
import { Button, TableContainer, TableHead, Table, TableRow, TableCell, TableBody, Divider, Dialog, DialogTitle, DialogContent, 
  TextField, DialogActions, IconButton } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import httpProvider from '../../components/httpProvider';

const url = "http://localhost:3100/api-cliente/producto";

class Productos extends React.Component {
  http = new httpProvider();
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
      show: false,
      openNot: false,
      ormProducto: { nombre: '', descripcion: '', precio_unitario: '' }
    };
    this.btnNuevo = this.btnNuevo.bind(this);
    this.btnEditar = this.btnEditar.bind(this);
    this.handleNombre = this.handleNombre.bind(this);
    this.handleDescripcion = this.handleDescripcion.bind(this);
    this.handlePrecio = this.handlePrecio.bind(this);
  }

  componentWillMount() {
    this.http.get(url).then(
      res => res.json()
    ).then((result) => {
      console.log(result);
      if(result.codigo === 0) {
        this.setState({
          isLoaded: true,
          data: result.data.lista
        });
      } else {
        this.setState({
          isLoaded: true,
          error: result.mensaje
        });
      }
    },
    (error) => {
      this.setState({
        isLoaded: true,
        error
      });
    });
  }

  render() {

    const { error, isLoaded, data } = this.state;
    if(error){
      return (<div> {error.message}</div>);
    } else if(!isLoaded){
      return (<CircularProgress />);
    } else {
      return (
        <div>
          <h1>Productos</h1>
          <Button variant="contained" startIcon={<AddIcon />} onClick={() => {this.btnNuevo()}}>Nuevo</Button>
          <NotificationContainer />
          <Divider />
          <TableContainer component={Paper}>
            <Table style={{minWidth: 400}} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Descripción</TableCell>
                  <TableCell align="right">Precio</TableCell>
                  <TableCell align="center">Creado</TableCell>
                  <TableCell align="center">Opciones</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row) => (
                  <TableRow key={row.productoid}>
                    <TableCell component="th" scope="row">{row.productoid}</TableCell>
                    <TableCell>{row.nombre}</TableCell>
                    <TableCell>{row.descripcion}</TableCell>
                    <TableCell align="right">{row.precio_unitario}</TableCell>
                    <TableCell align="center">{row.fecha_creado}</TableCell>
                    <TableCell align="center">
                      <IconButton onClick={() => {this.btnEditar(row)}}><EditIcon /></IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          
          <Dialog open={this.state.show} onClose={this.handleClose.bind(this)} aria-labelledby="form-dialog-title"
              disableBackdropClick disableEscapeKeyDown>
            <DialogTitle id="form-dialog-title">{this.state.formTitle}</DialogTitle>
            <DialogContent>
              <form>
                <div>
                  <TextField autoFocus value={this.state.ormProducto.nombre} name="txtNombre"
                      onChange={this.handleNombre} id="txtNombre" label="Nombre Producto" type="text" />
                </div><br/>
                <div>
                  <TextField value={this.state.ormProducto.descripcion} name="txtDescripcion"
                      onChange={this.handleDescripcion} id="txtDescripcion" label="Descripción Producto" type="text" />
                </div><br/>
                <div>
                  <TextField value={this.state.ormProducto.precio_unitario} name="txtPrecio"
                      onChange={this.handlePrecio} id="txtPrecio" label="Precio Producto" type="number" />
                </div>
              </form>
            </DialogContent>
            <DialogActions>
              <Button onClick={this.guardarProducto.bind(this)} color="primary">
                Guardar
              </Button>
              <Button onClick={this.handleClose.bind(this)} color="primary">
                Cerrar
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      );
    }
  }


  handleNombre(e) {
    let prod = this.state.ormProducto;
    prod.nombre = e.target.value;
    this.setState({
      ormProducto: prod
    });
  }
  handleDescripcion(e) {
    let prod = this.state.ormProducto;
    prod.descripcion = e.target.value;
    this.setState({
      ormProducto: prod
    });
  }
  handlePrecio(e) {
    let prod = this.state.ormProducto;
    prod.precio_unitario = e.target.value;
    this.setState({
      ormProducto: prod
    });
  }

  handleClose() {
    this.setState({show: false});
  }

  btnNuevo() {
    this.setState({
      formTitle: 'Nuevo Registro',
      show: true,
      ormProducto: { nombre: '', descripcion: '', precio_unitario: '' }
    });
  }

  btnEditar(row) {
    let registro = {
      productoid: row.productoid,
      nombre: row.nombre,
      descripcion: row.descripcion,
      precio_unitario: row.precio_unitario
    };
    this.setState({
      formTitle: 'Modificar Registro',
      show: true,
      ormProducto: registro
    });
  }

  guardarProducto() {
    const orm = this.state.ormProducto;

    console.log(orm);
    if(orm.productoid) {

      console.log('put');
      this.http.put(url, orm).then((result) => {
        console.log(result);
        if(result.codigo === 0) {
          NotificationManager.success('Información guardada exitosamente.');
          this.componentWillMount();
          this.handleClose();
        } else {
          NotificationManager.error(result.mensaje);
        }
      }, (error) => {
        console.log(error);
        NotificationManager.error('Ocurrió un evento inesperado.');
      });

    } else {

      console.log('post');
      this.http.post(url, orm).then((result) => {
        console.log(result);
        if(result.codigo === 0) {
          NotificationManager.success('Información guardada exitosamente.');
          this.componentWillMount();
          this.handleClose();
        } else {
          NotificationManager.error(result.mensaje);
        }
      }, (error) => {
        console.log(error);
        NotificationManager.error('Ocurrió un evento inesperado.');
      });

    }
  }

}  
export default Productos;
