import React from 'react';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { Button, TableContainer, TableHead, Table, TableRow, TableCell, TableBody, Divider, Dialog, DialogTitle, DialogContent, 
  TextField, DialogActions, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddIcon from '@material-ui/icons/Add';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import httpProvider from '../../components/httpProvider';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';

const url = "http://localhost:3100/api-cliente/venta";

const useRowStyles = makeStyles({
    root: {
      '& > *': {
        borderBottom: 'unset',
      },
    },
  });

function Row(props) {
    const { row, detalle } = props;
    const [open, setOpen] = React.useState(false);
    const classes = useRowStyles();
  
    return (
        <React.Fragment>
            <TableRow className={classes.root}>
                <TableCell>
                    <IconButton aria-label="expand row" size="small" onClick={() => {setOpen(!open); props.onOpen(row)}}>
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                    </IconButton>
                </TableCell>
                <TableCell component="th" scope="row">{row.ventaid}</TableCell>
                <TableCell>{row.nombre}</TableCell>
                <TableCell>{row.nit}</TableCell>
                <TableCell align="right">{row.total}</TableCell>
                <TableCell align="center">{row.fecha_creado}</TableCell>
            </TableRow>
            <TableRow>
                <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                    <Collapse in={open} timeout="auto" unmountOnExit>
                    <Box margin={1}>
                        <Typography variant="h6" gutterBottom component="div">
                            Detalle Venta
                        </Typography>
                        <Table size="small" aria-label="purchases">
                        <TableHead>
                            <TableRow>
                                <TableCell>No.</TableCell>
                                <TableCell>Producto</TableCell>
                                <TableCell align="right">Precio</TableCell>
                                <TableCell align="right">Cantidad</TableCell>
                                <TableCell align="right">Subtotal</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {detalle.map((det) => (
                                <TableRow key={det}>
                                    <TableCell component="th" scope="row">{det.ventadetid}</TableCell>
                                    <TableCell>{det.nombre}</TableCell>
                                    <TableCell align="right">{det.precio}</TableCell>
                                    <TableCell align="right">{det.cantidad}</TableCell>
                                    <TableCell align="right">
                                        {Math.round(det.cantidad * det.precio * 100) / 100}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                        </Table>
                    </Box>
                    </Collapse>
                </TableCell>
            </TableRow>
        </React.Fragment>
    );
  }

class Venta extends React.Component { 
    http = new httpProvider();
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: [],
            show: false,
            openNot: false,
            dataCliente: [],
            dataProducto: [],
            ormVenta: { ventaid: '', clienteid: '', total: '' },
            dataSeleccionado: {},
            precio: '', cantidad: '',
            detalleVenta: []
        };
        this.btnNuevo = this.btnNuevo.bind(this);
        // this.btnEditar = this.btnEditar.bind(this);
        this.handleCliente = this.handleCliente.bind(this);
        this.handleProducto = this.handleProducto.bind(this);
        this.handleTotal = this.handleTotal.bind(this);
        this.handleCantidad = this.handleCantidad.bind(this);
        this.getDetalle = this.getDetalle.bind(this);
    }

    componentWillMount() {
        this.http.get(url)
        .then(res => res.json())
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                let lista = result.data.lista;
                lista.forEach(element => {
                    element.detalle = [];
                });
                console.log(lista);
                this.setState({
                    isLoaded: true,
                    data: lista
                });
            } else {
                this.setState({
                isLoaded: true,
                error: result.mensaje
                });
            }
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            });
        });
    }

    render() {
        const { error, isLoaded, data, dataCliente, dataProducto, detalleVenta } = this.state;
        if(error){
            return (<div> {error.message}</div>);
        } else if(!isLoaded){
            return (<CircularProgress />);
        } else {
            return (
                <div>
                    <h1>Ventas</h1>
                    <Button variant="contained" startIcon={<AddIcon />} onClick={() => {this.btnNuevo()}}>Nuevo</Button>
                    <NotificationContainer />
                    <Divider />
                    <TableContainer component={Paper}>
                        <Table style={{minWidth: 400}} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell />
                                    <TableCell>ID</TableCell>
                                    <TableCell>Cliente</TableCell>
                                    <TableCell>Nit</TableCell>
                                    <TableCell align="right">Total Venta</TableCell>
                                    <TableCell align="center">Creado</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map((row) => (
                                    <Row row={row} detalle={row.detalle} onOpen={this.getDetalle}  />
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    
                    <Dialog open={this.state.show} onClose={this.handleClose.bind(this)} aria-labelledby="form-dialog-title"
                        disableBackdropClick disableEscapeKeyDown style={{height: 600}}>
                        <DialogTitle id="form-dialog-title">{this.state.formTitle}</DialogTitle>
                        <DialogContent>

                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <InputLabel id="lblCliente">Cliente:</InputLabel>
                                <Select labelId="lblCliente" id="cmbCliente" onChange={this.handleCliente}
                                    value={this.state.ormVenta.clienteid}>
                                    {dataCliente.map((fila, key) => (
                                        <MenuItem key={key} value={fila.clienteid}>{fila.nombre}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            
                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <TextField value={this.state.ormVenta.total} name="txtTotal"
                                    onChange={this.handleTotal} id="txtTotal" label="Total:" type="text" disabled="true" />
                            </FormControl><br /><br />

                            
                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <InputLabel id="lblProducto">Producto:</InputLabel>
                                <Select labelId="lblProducto" id="cmbProducto" onChange={this.handleProducto}
                                    value={this.state.dataSeleccionado?this.state.dataSeleccionado:''}>
                                    {dataProducto.map((fila, key) => (
                                        <MenuItem key={key} value={fila}>{fila.nombre}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            
                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <TextField value={this.state.precio} name="txtPrecio"
                                        id="txtPrecio" label="Precio:" type="text" disabled="true" />
                            </FormControl>

                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <TextField value={this.state.cantidad} name="txtCantidad" onChange={this.handleCantidad}
                                        id="txtCantidad" label="Cantidad:" type="number" />
                            </FormControl>

                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <Button variant="contained" onClick={this.btnAgregaProducto.bind(this)} color="primary">Agregar</Button>
                            </FormControl>

                            <Paper>
                                <TableContainer component={Paper}>
                                    <Table aria-label="simple table">
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>ID</TableCell>
                                                <TableCell>Producto</TableCell>
                                                <TableCell align="right">Precio</TableCell>
                                                <TableCell align="right">Cantidad</TableCell>
                                                <TableCell align="right">Total</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {detalleVenta.map((row) => (
                                            <TableRow key={row}>
                                                <TableCell component="th" scope="row">{row.productoid}</TableCell>
                                                <TableCell>{row.nombre}</TableCell>
                                                <TableCell align="right">{row.precio_unitario}</TableCell>
                                                <TableCell align="right">{row.cantidad}</TableCell>
                                                <TableCell align="right">{row.total}</TableCell>
                                            </TableRow>
                                            ))}
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Paper>

                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.guardarVenta.bind(this)} color="primary">Guardar</Button>
                            <Button onClick={this.handleClose.bind(this)} color="primary">Cerrar</Button>
                        </DialogActions>
                    </Dialog>
                </div>
            );
        }
    }

    handleCliente(e) {
        let venta = this.state.ormVenta;
        venta.clienteid = e.target.value;
        this.setState({
            ormVenta: venta
        });
    }
    handleNombre(e) {
        let prod = this.state.ormVenta;
        prod.nombre = e.target.value;
        this.setState({
            ormVenta: prod
        });
    }
    handleProducto(e) {
        console.log(e);
        const prod = e.target.value;
        this.setState({ dataSeleccionado: e.target.value, precio: prod.precio_unitario });
    }
    handleTotal(e) {
        this.setState({ total: e.target.value });
    }
    handleCantidad(e) {
        this.setState({ cantidad: e.target.value });
    }

    handleClose() {
        this.setState({
            show: false,
            openNot: false,
            dataCliente: [],
            dataProducto: [],
            ormVenta: { ventaid: '', clienteid: '', total: '' },
            dataSeleccionado: {},
            precio: '', cantidad: '',
            detalleVenta: []
        });
    }

    btnAgregaProducto() {
        let detalleVenta = this.state.detalleVenta;
        const orm = this.state.dataSeleccionado;
        if(!orm.productoid) {
            return;
        }
        let ormVenta = this.state.ormVenta;
        const total = parseFloat(this.state.cantidad) * parseFloat(orm.precio_unitario);
        ormVenta.total = parseFloat(ormVenta.total===''?0:ormVenta.total) + parseFloat(total);
        const det = {
            productoid: orm.productoid,
            nombre: orm.nombre,
            precio_unitario: orm.precio_unitario,
            cantidad: this.state.cantidad,
            total: total
        };
        detalleVenta.push(det);
        this.setState({
            ormVenta: ormVenta,
            detalleVenta: detalleVenta,
            dataSeleccionado: {}, precio: '', cantidad: ''
        });
    }

    getDetalle(e) {
        console.log('GetDetalle', e);
        this.http.get('http://localhost:3100/api-cliente/venta-det?ventaid='+e.ventaid)
        .then(res => res.json())
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                let data = this.state.data;
                let i=0;
                let found = false;
                while(i<data.length && !found) {
                    if(data[i].ventaid === e.ventaid) {
                        data[i].detalle = result.data.lista;
                        found = true;
                    }
                    i++;
                }
                console.log(data);
                this.setState({
                    data: data
                });
                // e.detalle = result.lista;
            } else {
                NotificationManager.error(result.mensaje);
            }
        }, (error) => {
            console.log(error);
            NotificationManager.error('Ocurrió un evento inesperado.');
        });
    }

    btnNuevo() {
        const urlCliente = "http://localhost:3100/api-cliente/clientes";
        
        this.http.get(urlCliente)
        .then(res => res.json())
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                this.setState({
                    dataCliente: result.data.lista
                });
                this.http.get("http://localhost:3100/api-cliente/producto")
                .then(res => res.json())
                .then((result) => {
                    console.log(result);
                    if(result.codigo === 0) {
                        this.setState({
                            formTitle: 'Nueva Venta',
                            show: true,
                            ormVenta: { ventaid: '', clienteid: '', total: '' },
                            dataProducto: result.data.lista
                        });
                    } else {
                        NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
                    }
                }, (error) => {
                    NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
                });
            } else {
                NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
            }
        }, (error) => {
            NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
        });
    }

    guardarVenta() {
        const detalleVenta = this.state.detalleVenta;
        let detalle = [];
        detalleVenta.map((det) => (
            detalle.push({
                productoid: det.productoid,
                precio: det.precio_unitario,
                cantidad: det.cantidad
            })
        ));
        
        const orm = this.state.ormVenta;
        const data = {
            clienteid: orm.clienteid,
            total: orm.total,
            detalle: detalle
        }
        console.log('Request', data);

        this.http.post(url, data).then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                NotificationManager.success('Datos de la venta almacenados correctamente.');
                this.componentWillMount();
                this.handleClose();
            } else {
                NotificationManager.error(result.mensaje);
            }
        }, (error) => {
            console.log(error);
            NotificationManager.error('Ocurrió un evento inesperado.');
        });
    }
}  
export default Venta;
