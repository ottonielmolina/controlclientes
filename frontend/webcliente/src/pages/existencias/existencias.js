import React from 'react';
import Paper from '@material-ui/core/Paper';
import { Button, TableContainer, TableHead, Table, TableRow, TableCell, TableBody, Divider, Dialog, DialogTitle, DialogContent, 
  TextField, DialogActions, IconButton, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import 'react-notifications/lib/notifications.css';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import httpProvider from '../../components/httpProvider';

const url = "http://localhost:3100/api-cliente/existencia";

class Existencias extends React.Component {  
    http = new httpProvider();
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: [],
            show: false,
            openNot: false,
            productoid: '',
            existencia: '',
            dataProducto: []
        };
        this.btnNuevo = this.btnNuevo.bind(this);
        this.btnEditar = this.btnEditar.bind(this);
        this.handleProducto = this.handleProducto.bind(this);
        this.handleExistencia = this.handleExistencia.bind(this);
    }

    componentWillMount() {
        this.http.get(url)
        .then(res => res.json())
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                this.setState({
                    isLoaded: true,
                    data: result.data.lista
                });
            } else {
                this.setState({
                    isLoaded: true,
                    error: result.mensaje
                });
            }
        },
        (error) => {
            this.setState({
                isLoaded: true,
                error
            });
        });
    }

    render() {
        const { error, isLoaded, data, dataProducto } = this.state;
        if(error){
            return (<div> {error.message}</div>);
        } else if(!isLoaded){
            return (<CircularProgress />);
        } else {
            return (
                <div>
                    <h1>Existencias</h1>
                    <Button variant="contained" startIcon={<AddIcon />} onClick={() => {this.btnNuevo()}}>Nuevo</Button>
                    <NotificationContainer />
                    <Divider />
                    <TableContainer component={Paper}>
                        <Table style={{minWidth: 400}} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell>ID</TableCell>
                                    <TableCell>Producto</TableCell>
                                    <TableCell align="right">Existencia</TableCell>
                                    <TableCell align="center">Opciones</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map((row) => (
                                    <TableRow key={row}>
                                        <TableCell component="th" scope="row">{row.productoid}</TableCell>
                                        <TableCell>{row.nombre}</TableCell>
                                        <TableCell align="right">{row.existencia}</TableCell>
                                        <TableCell align="center">
                                            <IconButton onClick={() => {this.btnEditar(row)}}><EditIcon /></IconButton>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                
                    <Dialog open={this.state.show} onClose={this.handleClose.bind(this)} aria-labelledby="form-dialog-title"
                        disableBackdropClick disableEscapeKeyDown>
                        <DialogTitle id="form-dialog-title">{this.state.formTitle}</DialogTitle>
                        <DialogContent>
                            
                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <InputLabel id="lblProducto">Producto:</InputLabel>
                                <Select labelId="lblProducto" id="cmbProducto" onChange={this.handleProducto}
                                    value={this.state.productoid}>
                                    {dataProducto.map((fila, key) => (
                                        <MenuItem key={key} value={fila.productoid}>{fila.nombre}</MenuItem>
                                    ))}
                                </Select>
                            </FormControl>

                            <FormControl style={{margin: 10, minWidth: 200}}>
                                <TextField value={this.state.existencia} name="txtCantidad" onChange={this.handleExistencia}
                                        id="txtCantidad" label="Cantidad:" type="number" />
                            </FormControl>

                        </DialogContent>
                        <DialogActions>
                        <Button onClick={this.guardarExistencia.bind(this)} color="primary">
                            Guardar
                        </Button>
                        <Button onClick={this.handleClose.bind(this)} color="primary">
                            Cerrar
                        </Button>
                        </DialogActions>
                    </Dialog>
                </div>
            );
        }
    }


    handleProducto(e) {
        this.setState({ productoid: e.target.value });
    }
    handleExistencia(e) {
        this.setState({ existencia: e.target.value });
    }

    handleClose() {
        this.setState({
            show: false,
            productoid: '',
            existencia: '',
        });
    }

    btnNuevo() {
        this.http.get("http://localhost:3100/api-cliente/producto")
        .then(res => res.json())
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                this.setState({
                    formTitle: 'Nuevo Registro',
                    show: true,
                    productoid: '',
                    existencia: '',
                    dataProducto: result.data.lista
                });
            } else {
                NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
            }
        }, (error) => {
            NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
        });
    }

    btnEditar(row) {
        this.http.get("http://localhost:3100/api-cliente/producto")
        .then(res => res.json())
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                console.log(row);
                this.setState({
                    formTitle: 'Modificar Registro',
                    show: true,
                    productoid: row.productoid,
                    existencia: '',
                    dataProducto: result.data.lista
                });
                
            } else {
                NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
            }
        }, (error) => {
            NotificationManager.error('En este momento no es posible generar una venta, reintente más tarde.');
        });
        
    }

    guardarExistencia() {
        const data = {
            productoid: this.state.productoid,
            existencia: this.state.existencia
        };

        this.http.post(url, data)
        .then((result) => {
            console.log(result);
            if(result.codigo === 0) {
                NotificationManager.success('Información guardada exitosamente.');
                this.handleClose();
                this.componentWillMount();
            } else {
                NotificationManager.error(result.mensaje);
            }
        }, (error) => {
            console.log(error);
            NotificationManager.error('Ocurrió un evento inesperado.');
        });
    }
}  
export default Existencias;
