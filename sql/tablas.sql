
-- Para registro de productos
create table tbl_producto(
	productoid integer primary key AUTO_INCREMENT,
	nombre varchar(128) not null,
	descripcion varchar(200) not null,
	precio_unitario decimal(10, 2) not null,
	fecha_creado datetime
);

-- Para registro de existencias de productos
create table tbl_existencia(
	productoid integer not null,
	existencia integer not null
);

create index tbl_existencia_idx on tbl_existencia (productoid);

-- Para registros de clientes
create table tbl_cliente(
	clienteid integer primary key AUTO_INCREMENT,
	nombre varchar(100) not null,
	nit varchar(10) not null,
	direccion varchar(250),
	fecha_creado datetime
);

create index tbl_cliente_nit_idx on tbl_cliente (nit);

-- Para registros de ventas
create table tbl_venta(
	ventaid integer primary key AUTO_INCREMENT,
	clienteid integer not null,
	total decimal(10, 2) not null,
	fecha_creado datetime not null
);

create index tbl_venta_clid_idx on tbl_venta (clienteid);

-- Para registro del detalle de venta
create table tbl_venta_det(
	ventaid integer not null,
	ventadetid integer not null,
	productoid integer not null,
	precio decimal(10, 2) not null,
	cantidad integer not null,
	primary key (ventaid, ventadetid)
);